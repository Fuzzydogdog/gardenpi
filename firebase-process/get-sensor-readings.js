const sensor = require('node-dht-sensor')

const getSensorReadings = (pin, callback) => {
    sensor.read(22, pin, function (err, temperature, humidity) {
        if (err) {
            return callback(err)
        }
    callback(null, temperature, humidity)
    })
}

    
module.exports = getSensorReadings
