const getSensorReadings = require('./get-sensor-readings')
var firebase = require('firebase')
require('firebase/firestore')

firebase.initializeApp({
    apiKey: 'AIzaSyCkrT1J-u8FMedyHKRtrP0EhTKuxJSNfRU',
    authDomain: 'gardenpi-598b1.firebaseapp.com',
    projectId: 'gardenpi-598b1'
})

const db = firebase.firestore()

setInterval(() => {
    getSensorReadings(4, (err, temperature, humidity) => {
        if (err) {
            return console.error(err)
        }
        db.collection("Sensors").doc("DHT22-1").update({
            Temperature: temperature,
            Humidity: humidity
        })
        .then(function(){
            console.log("DHT22-1 data updated")
        })
        .catch(function(error){
            console.error("Error updating DHT22-1: ", error)
        })
    })

    getSensorReadings(17, (err, temperature, humidity) => {
        if (err) {
            return console.error(err)
        }
        db.collection("Sensors").doc("DHT22-2").update({
            Temperature: temperature,
            Humidity: humidity
        })
        .then(function(){
            console.log("DHT22-2 data updated")
        })
        .catch(function(error){
            console.error("Error updating DHT22-2: ", error)
        })
    })
}, 4000)