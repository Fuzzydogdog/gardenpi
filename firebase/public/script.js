console.log('Executing client side javasript...')
const dht22_1Display = document.getElementById('DHT22-1-display')
const dht22_2Display = document.getElementById('DHT22-2-display')

const dht22_1_CanvasCtx = document.getElementById('DHT22-1-chart').getContext('2d')

const dht22_1_ChartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Temperature",
            yAxisID: "Temperature",
            data: [],
            backgroundColor: 'rgb(255, 0, 0)'
        },
        {
            label: "Humidity",
            yAxisID: "Humidity",
            data: [],
            backgroundColor: 'rgb(255, 0, 0)'
        }]
    },
    options: {
        legend: {
            display: false
        },
        responsive: true,
        maintainAspectRatio: false,
        stacked: false,
        scales: {
            yAxes: [{
                type: "linear",
                display: True,
                position: "left",
                id: "Humidity"
            },
            {
                type: "linear",
                display: true,
                position: "right",
                id: "Temperature"
            }]
        }
    }
}

const dht22_1_Chart = new Chart(dht22_1_CanvasCtx, dht22_1_ChartConfig)

const pushData = (arr, value, maxLen) => {
    arr.push(value)
    if (arr.length > maxLen) {
        arr.shift()
    }
}

/* const fetchTemperature = () =>{}

const fetchHumidity = () => {}

const fetchTemperatureHistory = () => {}

const fetchHumidityHistory = () => {}

function getParameterByName (name) {
    const url = window.location.href
    name = name.replace(/[\[\]]/g, '\\$&')
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
    const results = regex.exec(url)
    if (!results) return null
    if (!results[2]) return ''
    return decodeURIComponent(results[2].replace(/\+/g, ''))
}

const fetchTemperatureRange = () => {}

const fetchHumidityRange = () => {}

const addSocketListeners = () => {
    const socket = io()

    socket.on('new-temperature', data => {
        const now = new Date()
        const timeNow = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds()
        pushData(temperatureChartConfig.data.labels, timeNow, 10)
        pushData(temperatureChartConfig.data.datasets[0].data, data.value, 10)

        temperatureChart.update()
        temperatureDisplay.innerHTML = '<strong>' + data.value + '</strong>'
    })

    socket.on('new-humidity', data => {
        const now = new Date()
        const timeNow = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds()
        pushData(humidityChartConfig.data.labels, timeNow, 10)
        pushData(humidityChartConfig.data.datasets[0].data, data.value, 10)

        humidityChart.update()
        humidityDisplay.innerHTML = '<strong>' + data.value + '</strong>'
    })
} 

if (!getParameterByName('start') && !getParameterByName('end')) {
    setInterval(() => {
        fetchTemperature()
        fetchHumidity()
    }, 2000)
    fetchTemperatureHistory()
    fetchHumidityHistory()
} else {
    fetchTemperatureRange()
    fetchHumidityRange()
} */

const db = firebase.firestore()

db.collection('Sensors').doc('DHT22-1').onSnapshot(function(doc){
    const now = new Date()
    const timeNow = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds()
    
    pushData(dht22_1_ChartConfig.data.labels, timeNow, 10)
    pushData(dht22_1_ChartConfig.data.datasets[0].data, doc.data().Temperature, 10)
    pushData(dht22_1_ChartConfig.data.datasets[1].data, doc.data().Humidity, 10)
    dht22_1_Chart.update()
    
    dht22_1Display.innerHTML = '<strong>' + doc.data().Temperature + "°C" + '<br>' + doc.data().Humidity + "%" + '</strong>'
})